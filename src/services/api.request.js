import axios from './axios';
import {getData} from '../services/localstorage';

const API_KEY = 'AIzaSyAVLAhLZ2blCZrScG6T7-VDpWFATzJXR1s';

export const getVideos = async (searchTerm) => {
    let response = await axios.get(`/search?part=snippet&maxResults=25&q=${searchTerm}&key=${API_KEY}`,
    {
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
    return response;
}