export const setData = (key,data) => {
    localStorage.setItem(key,JSON.stringify(data))
}

export const getData = (key) => {
    try{
        return JSON.parse(localStorage.getItem(key));
    }catch(e){
        return ''
    }
    
}
