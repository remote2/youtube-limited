import { Component,createRef } from 'react';
import './App.css';
import Header from './components/Header';
import SearchResults from './components/SearchResults';

export default class App extends Component {
  constructor(props) {
    super(props)
    this.searchResultsRef = createRef();
  }
  componentDidMount(){
  }
  render() {
    return (
      <div className="App">
        <Header onSearch={(key)=>this.onSearch(key)} />
        <SearchResults ref={this.searchResultsRef}/>
      </div>
    );
  }
  onSearch(key) {
    console.log(this.searchResultsRef)
    return this.searchResultsRef.current.getYtVideos(key);
  }

}
