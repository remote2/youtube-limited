import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';
import googleIcon from '../assets/google.png';
import { setData } from '../services/localstorage';

export class Header extends Component {
    state = {
        searchTerm: '',
    }
    render() {
        return (
            <div>
                <div className="header p-3 d-flex align-items-center">
                    <span className="brand">Youtube</span>
                    <div className="col-9">
                        <input className="form-control form-control-lg search ml-5" type="search" onChange={this.handleChange} onKeyDown={(e)=>{return e.keyCode==13?this.props.onSearch(this.state.searchTerm):undefined}}
                            placeholder="Search Videos"></input>
                    </div>
                    <div className="col-2 text-center">
                        <GoogleLogin
                            clientId="424603004979-96ufma6qga40o8gapflki75qnaifuhp8.apps.googleusercontent.com"
                            buttonText="Login"
                            render={renderProps => (
                                <img src={googleIcon} className="google__icon" onClick={renderProps.onClick} disabled={renderProps.disabled} />
                            )}
                            onSuccess={this.responseGoogle}
                            onFailure={this.responseGoogle}
                            cookiePolicy={'single_host_origin'}
                        />
                    </div>

                </div>
            </div>
        )
    }

    responseGoogle = (response) => {
        console.log(response);
        if(response) {setData("GOOGLE_DATA", response);}
    }

    handleChange = (event) => {
        this.setState({ searchTerm: event.target.value });
    }
}

export default Header
