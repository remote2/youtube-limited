import React, { Component } from 'react';
import { getVideos } from '../services/api.request';
import { getData } from '../services/localstorage';
import searchImage from '../assets/search.png';

export class SearchResults extends Component {
    state = {
        videos: []
    }
    componentDidMount() {
    }

    render() {
        return (
            <div className="p-5">
                {this.state.videos.length > 0 ? this.renderVideos() : (
                    <div className="text-center">
                        <h2 className="ml-3 mt-3 text-secondary">Enter something in searchbox and press enter</h2>
                        <img src={searchImage} className="noresult" />
                    </div>
                )}
            </div>
        )
    }
    renderVideos() {
        return (
            <div>
                <h2 className="ml-3 mb-5 text-secondary">Your Search Result</h2>
                {
                    this.state.videos.map(item =>
                        <div key={item.etag} className="d-flex row search__item justify-content-center">

                            <div className="col-md-4 col-sm-8">
                                <img src={item.snippet.thumbnails.medium.url} className="thumbnail" />
                            </div>
                            <div className="col-md-8 col-sm-12 py-4">
                                <h4>{item.snippet.title}</h4>
                                <span>{item.snippet.channelTitle}</span>
                                <p className="mt-3">{item.snippet.description}</p>
                            </div>

                        </div>
                    )
                }
            </div>
        )
    }

    getYtVideos(searchTerm) {
        getVideos(searchTerm).then(response => {
            console.log("result", response);
            this.setState({ videos: response.data.items })
        })
    }
}

export default SearchResults
